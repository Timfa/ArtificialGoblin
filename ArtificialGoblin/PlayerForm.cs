﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ArtificialGoblin
{
    public partial class PlayerForm : Form
    {
        public bool IgnoreMove = false;
        public Form1 MainForm;

        private List<int> damageDone = new List<int>();

        public int AverageDamage
        {
            get
            {
                if(damageDone.Count == 0)
                    return 0;

                int total = 0;
                int amount = 0;

                for(int i = 0; i < damageDone.Count; i++)
                {
                    amount++;
                    total += damageDone[i];
                }

                return (int)Math.Ceiling((double)total / (double)amount);
            }
        }

        public PlayerForm()
        {
            InitializeComponent();
        }

        public void EndCombat()
        {
            damageDone = new List<int>();
        }

        private void playerName_TextChanged(object sender, EventArgs e)
        {
            Text = playerName.Text;
        }

        private void playerAC_TextChanged(object sender, EventArgs e)
        {
            playerAC.Text = Regex.Match(playerAC.Text, "[0-9]+").ToString();
        }

        private void damageInput_TextChanged(object sender, EventArgs e)
        {
            damageInput.Text = Regex.Match(damageInput.Text, "[0-9]+").ToString();
        }

        private void applyDamageButton_Click(object sender, EventArgs e)
        {
            int damage = -1;

            int.TryParse(Regex.Match(damageInput.Text, "[0-9]+").ToString(), out damage);

            if(damage > 0)
            {
                damageDone.Add(damage);
            }

            damageInput.Text = "";

            avgDamage.Text = AverageDamage.ToString();
        }

        public PlayerStats GetStats()
        {
            return new PlayerStats(playerName.Text,
                int.Parse(Regex.Match(hpTextbox.Text, "[0-9]+").ToString()),
                int.Parse(Regex.Match(playerAC.Text, "[0-9]+").ToString()), 
                AverageDamage);
        }

        internal void SetName()
        {
            Text = playerName.Text;
        }

        public struct PlayerStats
        {
            public string Name;
            public int HP, AC, AvgDamage;

            public PlayerStats(string name, int hp, int ac, int avgDamage)
            {
                Name = name;
                HP = hp;
                AC = ac;
                AvgDamage = avgDamage;
            }
        }

        private void PlayerForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            MainForm.Unregister(this);
            MainForm.Focus();
        }

        private void hpTextbox_TextChanged(object sender, EventArgs e)
        {
            hpTextbox.Text = Regex.Match(hpTextbox.Text, "[0-9]+").ToString();
        }

        private void PlayerForm_Move(object sender, EventArgs e)
        {
            if (!IgnoreMove)
                Form1.Instance.PlayerMoved(this);
        }
    }
}
