﻿namespace ArtificialGoblin
{
    partial class PlayerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.playerName = new System.Windows.Forms.TextBox();
            this.playerAC = new System.Windows.Forms.TextBox();
            this.applyDamageButton = new System.Windows.Forms.Button();
            this.damageInput = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.avgDamage = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.hpTextbox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // playerName
            // 
            this.playerName.Location = new System.Drawing.Point(50, 13);
            this.playerName.Name = "playerName";
            this.playerName.Size = new System.Drawing.Size(100, 20);
            this.playerName.TabIndex = 0;
            this.playerName.Text = "Player";
            this.playerName.TextChanged += new System.EventHandler(this.playerName_TextChanged);
            // 
            // playerAC
            // 
            this.playerAC.Location = new System.Drawing.Point(50, 40);
            this.playerAC.Name = "playerAC";
            this.playerAC.Size = new System.Drawing.Size(100, 20);
            this.playerAC.TabIndex = 1;
            this.playerAC.Text = "10";
            this.playerAC.TextChanged += new System.EventHandler(this.playerAC_TextChanged);
            // 
            // applyDamageButton
            // 
            this.applyDamageButton.Location = new System.Drawing.Point(11, 158);
            this.applyDamageButton.Name = "applyDamageButton";
            this.applyDamageButton.Size = new System.Drawing.Size(140, 23);
            this.applyDamageButton.TabIndex = 2;
            this.applyDamageButton.Text = "Deal Damage";
            this.applyDamageButton.UseVisualStyleBackColor = true;
            this.applyDamageButton.Click += new System.EventHandler(this.applyDamageButton_Click);
            // 
            // damageInput
            // 
            this.damageInput.Location = new System.Drawing.Point(11, 132);
            this.damageInput.Name = "damageInput";
            this.damageInput.Size = new System.Drawing.Size(140, 20);
            this.damageInput.TabIndex = 3;
            this.damageInput.TextChanged += new System.EventHandler(this.damageInput_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "AC";
            // 
            // avgDamage
            // 
            this.avgDamage.AutoSize = true;
            this.avgDamage.Location = new System.Drawing.Point(89, 113);
            this.avgDamage.Name = "avgDamage";
            this.avgDamage.Size = new System.Drawing.Size(13, 13);
            this.avgDamage.TabIndex = 6;
            this.avgDamage.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Avg. Damage";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(22, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "HP";
            // 
            // hpTextbox
            // 
            this.hpTextbox.Location = new System.Drawing.Point(51, 66);
            this.hpTextbox.Name = "hpTextbox";
            this.hpTextbox.Size = new System.Drawing.Size(100, 20);
            this.hpTextbox.TabIndex = 8;
            this.hpTextbox.Text = "10";
            this.hpTextbox.TextChanged += new System.EventHandler(this.hpTextbox_TextChanged);
            // 
            // PlayerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(163, 192);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.hpTextbox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.avgDamage);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.damageInput);
            this.Controls.Add(this.applyDamageButton);
            this.Controls.Add(this.playerAC);
            this.Controls.Add(this.playerName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "PlayerForm";
            this.Text = "Player";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.PlayerForm_FormClosed);
            this.Move += new System.EventHandler(this.PlayerForm_Move);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox playerName;
        private System.Windows.Forms.TextBox playerAC;
        private System.Windows.Forms.Button applyDamageButton;
        private System.Windows.Forms.TextBox damageInput;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label avgDamage;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox hpTextbox;
    }
}