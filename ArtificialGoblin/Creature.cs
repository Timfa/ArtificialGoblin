﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ArtificialGoblin
{
    public partial class CreatureDialog : Form
    {
        public bool IgnoreMove = false;
        public Form1 MainForm;

        public CreatureData creatureData = new CreatureData();
        private NeuralNetwork.Network Brain;
        private double fitnessToBeat = 0;

        private CreatureConfigurer configurer;

        private int selectedAction = -1;

        private List<int> damageDone = new List<int>();

        string FileName
        {
            get
            {
                return creatureData.Name.ToLower() + ".json";
            }
        }

        public int AverageDamage
        {
            get
            {
                if(damageDone.Count == 0)
                    return 0;

                int total = 0;
                int amount = 0;

                for(int i = 0; i < damageDone.Count; i++)
                {
                    amount++;
                    total += damageDone[i];
                }

                return (int)Math.Ceiling((double)total / (double)amount);
            }
        }

        public CreatureDialog()
        {
            InitializeComponent();
            SetupBrain();
            ApplyDataToForm();

            successButton.Enabled = false;
            failureButton.Enabled = false;
            neutralButton.Enabled = false;
        }

        public void SetFormName()
        {
            Text = creatureName.Text + " Intelligence: " + Brain.Fitness + "/" + fitnessToBeat;
        }

        private void loadButton_Click(object sender, EventArgs e)
        {
            LoadDialog loadDialog = new LoadDialog();
            loadDialog.creatureDialog = this;
            loadDialog.ShowDialog();
        }

        private void ApplyDataToForm()
        {
            creatureName.Text = creatureData.Name;
            hpLabel.Text = creatureData.MaxHP.ToString();

            SetFormName();
        }

        public void LoadCreature(CreatureData data)
        {
            creatureData = data;

            Brain = NeuralNetwork.Network.Parse(creatureData.BrainString);

            fitnessToBeat = Brain.Fitness;

            Brain.Mutate(creatureData.MutationRate);

            save.Enabled = false;

            ApplyDataToForm();
        }

        private void Creature_FormClosed(object sender, FormClosedEventArgs e)
        {
            MainForm.Unregister(this);
            MainForm.Focus();
        }

        public void SetupBrain()
        {
            Brain = new NeuralNetwork.Network(6);
            Brain.AddLayer(5, NeuralNetwork.Neuron.ActivationTypes.Linear);
            Brain.AddLayer(10, NeuralNetwork.Neuron.ActivationTypes.Linear);
            Brain.AddLayer(15, NeuralNetwork.Neuron.ActivationTypes.Linear);
            Brain.AddLayer(10, NeuralNetwork.Neuron.ActivationTypes.Linear);
            Brain.AddLayer(5, NeuralNetwork.Neuron.ActivationTypes.Linear);

            Brain.AddLayer(creatureData.Actions.Length, NeuralNetwork.Neuron.ActivationTypes.Linear);
        }

        public void IncreaseFitness(double fitness)
        {
            AlterFitness(fitness);
        }

        public void DecreaseFitness(double fitness)
        {
            AlterFitness(-fitness);
        }

        public void AlterFitness(double fitness)
        {
            Brain.Fitness += fitness;

            SetSaveButton();

            save.Enabled = true;

            SetFormName();
        }

        public class CreatureData
        {
            public string Name = "Goblin";
            public int MaxHP = 7;
            public int HP = 7;
            public string BrainString;

            public int DeathPenalty = 10;
            public int DamagePenalty = 1;

            public Action[] Actions = new Action[]
            {
                new Action("Attack %T", 5, 2),
                new Action("Distract %T", 6, 4),
                new Action("Flee from %T", 3, 2),
                new Action("Guard from %T", 5, 2),
                new Action("Disengage Combat", 10, 5)
            };

            public double MutationRate = 3.5;

            public class Action
            {
                public string Name;
                public int SuccessReward, FailurePenalty;
                public string TargetName = "Party";

                public Action(string name, int success, int failure)
                {
                    Name = name;
                    SuccessReward = success;
                    FailurePenalty = failure;
                }

                public Action()
                {
                    Name = "Action for %T";
                    SuccessReward = 0;
                    FailurePenalty = 0;
                }

                public override string ToString()
                {
                    return Regex.Replace(Name, "%T", TargetName);
                }
            }

            public bool EqualsFile(string name)
            {
                CreatureData data = DataSaver.LoadFile<CreatureData>(name);

                if(data == null)
                    return false;

                bool equal = true;

                if(data.Actions.Length != Actions.Length)
                {
                    equal = false;
                }

                return equal;
            }
        }

        private void creatureName_TextChanged(object sender, EventArgs e)
        {
            creatureData.Name = creatureName.Text;
            SetFormName();
            SetSaveButton();
        }

        public void SetSaveButton()
        {
            string name = creatureData.Name.ToLower() + ".json";

            bool equal = creatureData.EqualsFile(name);

            if (DataSaver.Files.Contains(name))
            {
                creatureData.EqualsFile(name);
                
                if (equal)
                {
                    save.Text = "Update";
                }
                else
                {
                    save.Text = "Overwrite";
                }
            }
            else
            {
                save.Text = "Save";
            }

            save.Enabled = true;
        }

        private void save_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void Save()
        {
            if(creatureData.EqualsFile(FileName))
            {
                //Adopt existing brain if it was better
                CreatureData existing = DataSaver.LoadFile<CreatureData>(FileName);

                NeuralNetwork.Network existingBrain = NeuralNetwork.Network.Parse(existing.BrainString);

                if(existingBrain.Fitness > Brain.Fitness)
                    Brain = existingBrain;
            }

            creatureData.BrainString = Brain.ToString();

            creatureData.SaveAsFile(FileName);

            save.Enabled = false;

            MainForm.ReloadList();
        }

        private void getActionButton_Click(object sender, EventArgs e)
        {
            PlayerForm.PlayerStats[] stats = MainForm.GetPlayerStats();

            if(stats.Length == 0)
            {
                return;
            }

            successButton.Enabled = true;
            failureButton.Enabled = true;
            neutralButton.Enabled = true;
            getActionButton.Enabled = false;

            string playerName = "";
            double strongest = double.MinValue;

            for(int i = 0; i < stats.Length; i++)
            {
                double[] output = Brain.GetOutput(new double[] { creatureData.HP, stats[i].HP, stats[i].AC, stats[i].AvgDamage, MainForm.Creatures.Count, AverageDamage });
                
                for(int a = 0; a < output.Length; a++)
                {
                    if (output[a] > strongest)
                    {
                        playerName = stats[i].Name;
                        strongest = output[a];
                        selectedAction = a;
                    }

                    Debug.Log("BRAIN: " + Math.Round(output[a], 2) + " " + creatureData.Actions[a].ToString());
                }
            }

            Debug.Log("BRAIN: " + creatureData.Actions[selectedAction].ToString());

            CreatureData.Action action = creatureData.Actions[selectedAction];

            action.TargetName = playerName;

            actionLabel.Text = action.ToString();
        }

        private void healInput_TextChanged(object sender, EventArgs e)
        {
            healInput.Text = Regex.Match(healInput.Text, "[0-9]+").ToString();
        }

        private void damageInput_TextChanged(object sender, EventArgs e)
        {
            damageInput.Text = Regex.Match(damageInput.Text, "[0-9]+").ToString();
        }

        private void doHeal_Click(object sender, EventArgs e)
        {
            try
            {
                int amount = int.Parse(healInput.Text);

                creatureData.HP += amount;

                hpLabel.Text = creatureData.HP.ToString();

                EnableControl(true);
            }
            catch
            {

            }
        }

        private void doDamage_Click(object sender, EventArgs e)
        {
            try
            {
                int amount = int.Parse(damageInput.Text);

                creatureData.HP -= amount;

                if(creatureData.HP > 0)
                {
                    DecreaseFitness(creatureData.DamagePenalty);
                }
                else
                {
                    DecreaseFitness(creatureData.DeathPenalty);

                    creatureData.HP = 0;

                    EnableControl(false);
                }

                SetFormName();

                hpLabel.Text = creatureData.HP.ToString();
            }
            catch { }
        }

        private void EnableControl(bool enable)
        {
            doDamage.Enabled = enable;
            getActionButton.Enabled = enable;
            successButton.Enabled = selectedAction != -1? enable : false;
            failureButton.Enabled = selectedAction != -1 ? enable : false;
        }

        private void successButton_Click(object sender, EventArgs e)
        {
            IncreaseFitness(creatureData.Actions[selectedAction].SuccessReward);

            successButton.Enabled = false;
            failureButton.Enabled = false;
            neutralButton.Enabled = false;
            getActionButton.Enabled = true;

            SetFormName();
        }

        private void failureButton_Click(object sender, EventArgs e)
        {
            DecreaseFitness(creatureData.Actions[selectedAction].FailurePenalty);

            successButton.Enabled = false;
            failureButton.Enabled = false;
            neutralButton.Enabled = false;
            getActionButton.Enabled = true;

            SetFormName();
        }

        private void configure_Click(object sender, EventArgs e)
        {
            configurer = new CreatureConfigurer(this);

            configurer.Text = creatureData.Name + " Configuration";

            configurer.ShowDialog();
        }

        private void damageDealtInput_TextChanged(object sender, EventArgs e)
        {
            damageDealtInput.Text = Regex.Match(damageDealtInput.Text, "[0-9]+").ToString();
        }

        private void dealDamageButton_Click(object sender, EventArgs e)
        {
            int damage = -1;

            int.TryParse(Regex.Match(damageDealtInput.Text, "[0-9]+").ToString(), out damage);

            if(damage > 0)
            {
                damageDone.Add(damage);
            }

            damageInput.Text = "";

            avgDamageLabel.Text = AverageDamage.ToString();
        }

        private void neutralButton_Click(object sender, EventArgs e)
        {
            successButton.Enabled = false;
            failureButton.Enabled = false;
            neutralButton.Enabled = false;
            getActionButton.Enabled = true;
        }

        private void CreatureDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (save.Enabled && (e.CloseReason == CloseReason.UserClosing) || e.CloseReason == CloseReason.ApplicationExitCall)
            {
                DialogResult dialogResult = MessageBox.Show(save.Text + " " + FileName + "?", "Save creature before closing?", MessageBoxButtons.YesNo);
                if(dialogResult == DialogResult.Yes)
                {
                    Save();
                }
            }
        }

        private void CreatureDialog_Move(object sender, EventArgs e)
        {
            if (!IgnoreMove)
                Form1.Instance.CreatureMoved(this);
        }
    }
}
