﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace ArtificialGoblin
{
    
    public partial class Form1 : Form
    {
        public List<CreatureDialog> Creatures = new List<CreatureDialog>();
        public List<PlayerForm> Players = new List<PlayerForm>();

        private int playerAdd = 0;

        public int MaxCreaturesWidth
        {
            get
            {
                if(Creatures.Count > 0)
                {
                    int pW = Screen.PrimaryScreen.Bounds.Width;

                    return (int)((pW / Creatures[0].Width) / 2);
                }

                return 1;
            }
        }

        public int MaxPlayersWidth
        {
            get
            {
                if (Players.Count > 0)
                {
                    int pW = Screen.PrimaryScreen.Bounds.Width;

                    return (int)((pW / Players[0].Width) / 2);
                }

                return 1;
            }
        }

        public static Form1 Instance { get; private set; }

        public Form1()
        {
            InitializeComponent();
            ReloadList();

            Instance = this;
        }

        public void ReloadList()
        {
            string[] fileNames = DataSaver.Files;

            fileList.DataSource = fileNames;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CreatureDialog creature = new CreatureDialog();
            creature.MainForm = this;
            creature.Show();

            Creatures.Add(creature);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void addPlayerButton_Click(object sender, EventArgs e)
        {
            PlayerForm player = new PlayerForm();
            player.MainForm = this;
            player.Show();

            Players.Add(player);

            playerAdd++;

            player.playerName.Text = "Player " + playerAdd;
            player.SetName();
            SetPlayerPositions();
        }

        public PlayerForm.PlayerStats[] GetPlayerStats()
        {
            List<PlayerForm.PlayerStats> stats = new List<PlayerForm.PlayerStats>();

            for(int i = 0; i < Players.Count; i++)
            {
                stats.Add(Players[i].GetStats());
            }

            return stats.ToArray();
        }

        public void Unregister(CreatureDialog creature)
        {
            Creatures.Remove(creature);
        }

        public void Unregister(PlayerForm player)
        {
            Players.Remove(player);
            SetPlayerPositions();
        }

        private void LoadSelectedCreature()
        {
            CreatureDialog creature = new CreatureDialog();
            creature.MainForm = this;
            creature.Show();

            Creatures.Add(creature);

            CreatureDialog.CreatureData data = DataSaver.LoadFile<CreatureDialog.CreatureData>((string)fileList.SelectedItem);
            creature.LoadCreature(data);
        }

        private void loadButton_Click(object sender, EventArgs e)
        {
            LoadSelectedCreature();   
        }

        private void fileList_DoubleClick(object sender, EventArgs e)
        {
            LoadSelectedCreature();
        }

        private void fileList_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                Process.Start(DataSaver.SaveFolder);
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Process.Start(DataSaver.SaveFolder);
        }

        internal void PlayerMoved(PlayerForm playerForm)
        {
            int movedIndex = Players.IndexOf(playerForm);

            if (movedIndex < 0)
                return;

            for(int i = 0; i < Players.Count; i++)
            {
                if (i != movedIndex)
                {
                    Players[i].IgnoreMove = true;
                    Players[i].Top = Players[movedIndex].Top;
                    Players[i].Left = Players[movedIndex].Left + (Players[i].Width * (i - movedIndex));
                    Players[i].IgnoreMove = false;
                }
            }
        }

        internal void SetPlayerPositions()
        {
            for (int i = 0; i < Players.Count; i++)
            {
                if (i != 0)
                {
                    Players[i].IgnoreMove = true;
                    Players[i].Top = Players[0].Top;
                    Players[i].Left = Players[0].Left + (Players[i].Width * (i));
                    Players[i].IgnoreMove = false;
                }
            }
        }

        internal void CreatureMoved(CreatureDialog playerForm)
        {
            return; //Disabeled intentionally for testing
            int movedIndex = Creatures.IndexOf(playerForm);

            if (movedIndex < 0)
                return;

            for (int i = 0; i < Creatures.Count; i++)
            {
                if (i != movedIndex)
                {
                    Creatures[i].IgnoreMove = true;

                    int heightIndex = (int)Math.Floor((double)i / (double)MaxCreaturesWidth);
                    int movedHeightIndex = (int)Math.Floor((double)movedIndex / (double)MaxCreaturesWidth);

                    Creatures[i].Top = Creatures[movedIndex].Top + (Creatures[i].Height * (heightIndex - movedHeightIndex));

                    Creatures[i].Left = Creatures[movedIndex].Left + (Creatures[i].Width * ((i  - movedIndex) ));
                   
                    Creatures[i].IgnoreMove = false;
                }
            }
        }

        internal void SetCreaturePositions()
        {
            return; //Disabeled intentionally for testing
            for (int i = 0; i < Creatures.Count; i++)
            {
                if (i != 0)
                {
                    Creatures[i].IgnoreMove = true;
                    Creatures[i].Top = Creatures[0].Top;
                    Creatures[i].Left = Creatures[0].Left + (Creatures[i].Width * (i));
                    Creatures[i].IgnoreMove = false;
                }
            }
        }
    }
}
