﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ArtificialGoblin
{
    public partial class CreatureConfigurer : Form
    {
        public CreatureDialog Creature;

        public CreatureConfigurer(CreatureDialog creature)
        {
            InitializeComponent();

            actionList.DataSource = creature.creatureData.Actions;

            Creature = creature;

            maxHP.Text = Creature.creatureData.MaxHP.ToString();
            deathPenalty.Text = Creature.creatureData.DeathPenalty.ToString();
            damagePenalty.Text = Creature.creatureData.DamagePenalty.ToString();
            mutationRate.Text = Creature.creatureData.MutationRate.ToString();
        }

        private void CreatureConfigurer_FormClosed(object sender, FormClosedEventArgs e)
        {
            Creature.SetSaveButton();
        }

        private void actionList_SelectedIndexChanged(object sender, EventArgs e)
        {
            CreatureDialog.CreatureData.Action action = ((CreatureDialog.CreatureData.Action)actionList.SelectedItem);

            if(action == null)
                return;

            actionName.Text = action.Name;
            successReward.Text = action.SuccessReward.ToString();
            failurePenalty.Text = action.FailurePenalty.ToString();
        }

        private void deleteAction_Click(object sender, EventArgs e)
        {
            CreatureDialog.CreatureData.Action action = ((CreatureDialog.CreatureData.Action)actionList.SelectedItem);

            List<CreatureDialog.CreatureData.Action> actions = new List<CreatureDialog.CreatureData.Action>();

            actions.AddRange(Creature.creatureData.Actions);

            actions.Remove(action);

            Creature.creatureData.Actions = actions.ToArray();

            actionList.DataSource = Creature.creatureData.Actions;

            Creature.SetupBrain();
        }

        private void addAction_Click(object sender, EventArgs e)
        {
            List<CreatureDialog.CreatureData.Action> actions = new List<CreatureDialog.CreatureData.Action>();

            actions.AddRange(Creature.creatureData.Actions);

            actions.Add(new CreatureDialog.CreatureData.Action());

            Creature.creatureData.Actions = actions.ToArray();

            actionList.DataSource = Creature.creatureData.Actions;

            Creature.SetupBrain();
        }

        private void saveAction_Click(object sender, EventArgs e)
        {
            CreatureDialog.CreatureData.Action action = ((CreatureDialog.CreatureData.Action)actionList.SelectedItem);

            action.Name = actionName.Text;
            action.SuccessReward = int.Parse(successReward.Text);
            action.FailurePenalty = int.Parse(failurePenalty.Text);
            
            actionList.DataSource = null;
            actionList.DataSource = Creature.creatureData.Actions;
        }

        private void successReward_TextChanged(object sender, EventArgs e)
        {
            successReward.Text = Regex.Match(successReward.Text, "[0-9]+").ToString();
        }

        private void failurePenalty_TextChanged(object sender, EventArgs e)
        {
            failurePenalty.Text = Regex.Match(failurePenalty.Text, "[0-9]+").ToString();
        }

        private void deathPenalty_TextChanged(object sender, EventArgs e)
        {
            try
            {
                deathPenalty.Text = Regex.Match(deathPenalty.Text, "[0-9]+").ToString();
                Creature.creatureData.DeathPenalty = int.Parse(deathPenalty.Text);
            } catch { }
        }

        private void damagePenalty_TextChanged(object sender, EventArgs e)
        {
            try
            { 
                damagePenalty.Text = Regex.Match(damagePenalty.Text, "[0-9]+").ToString();
                Creature.creatureData.DamagePenalty = int.Parse(damagePenalty.Text);
            }
            catch { }
        }

        private void maxHP_TextChanged(object sender, EventArgs e)
        {
            try
            { 
                maxHP.Text = Regex.Match(maxHP.Text, "[0-9]+").ToString();
                Creature.creatureData.MaxHP = int.Parse(maxHP.Text);
            }
            catch { }
        }

        private void mutationRate_TextChanged(object sender, EventArgs e)
        {
            try
            { 
                mutationRate.Text = Regex.Match(mutationRate.Text, @"[0-9]*\.?[0-9]*").ToString();
            }
            catch { }
        }
    }
}
