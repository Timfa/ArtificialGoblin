﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ArtificialGoblin
{
    public partial class LoadDialog : Form
    {
        public CreatureDialog creatureDialog;

        public LoadDialog()
        {
            InitializeComponent();

            string[] fileNames = DataSaver.Files;

            fileList.DataSource = fileNames;
        }

        private void loadButton_Click(object sender, EventArgs e)
        {
            CreatureDialog.CreatureData data = DataSaver.LoadFile<CreatureDialog.CreatureData>((string)fileList.SelectedItem);
            creatureDialog.LoadCreature(data);

            Close();
        }
    }
}
