﻿namespace ArtificialGoblin
{
    partial class CreatureConfigurer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.actionList = new System.Windows.Forms.ListBox();
            this.maxHP = new System.Windows.Forms.TextBox();
            this.deathPenalty = new System.Windows.Forms.TextBox();
            this.damagePenalty = new System.Windows.Forms.TextBox();
            this.actionName = new System.Windows.Forms.TextBox();
            this.successReward = new System.Windows.Forms.TextBox();
            this.failurePenalty = new System.Windows.Forms.TextBox();
            this.deleteAction = new System.Windows.Forms.Button();
            this.saveAction = new System.Windows.Forms.Button();
            this.addAction = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.mutationRate = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Success = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Max HP";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Death Intelligence Penalty";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(142, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Damage Intelligence Penalty";
            // 
            // actionList
            // 
            this.actionList.FormattingEnabled = true;
            this.actionList.Location = new System.Drawing.Point(16, 140);
            this.actionList.Name = "actionList";
            this.actionList.Size = new System.Drawing.Size(128, 121);
            this.actionList.TabIndex = 3;
            this.actionList.SelectedIndexChanged += new System.EventHandler(this.actionList_SelectedIndexChanged);
            // 
            // maxHP
            // 
            this.maxHP.Location = new System.Drawing.Point(172, 13);
            this.maxHP.Name = "maxHP";
            this.maxHP.Size = new System.Drawing.Size(100, 20);
            this.maxHP.TabIndex = 4;
            this.maxHP.TextChanged += new System.EventHandler(this.maxHP_TextChanged);
            // 
            // deathPenalty
            // 
            this.deathPenalty.Location = new System.Drawing.Point(172, 40);
            this.deathPenalty.Name = "deathPenalty";
            this.deathPenalty.Size = new System.Drawing.Size(100, 20);
            this.deathPenalty.TabIndex = 5;
            this.deathPenalty.TextChanged += new System.EventHandler(this.deathPenalty_TextChanged);
            // 
            // damagePenalty
            // 
            this.damagePenalty.Location = new System.Drawing.Point(172, 66);
            this.damagePenalty.Name = "damagePenalty";
            this.damagePenalty.Size = new System.Drawing.Size(100, 20);
            this.damagePenalty.TabIndex = 6;
            this.damagePenalty.TextChanged += new System.EventHandler(this.damagePenalty_TextChanged);
            // 
            // actionName
            // 
            this.actionName.Location = new System.Drawing.Point(150, 140);
            this.actionName.Name = "actionName";
            this.actionName.Size = new System.Drawing.Size(122, 20);
            this.actionName.TabIndex = 7;
            // 
            // successReward
            // 
            this.successReward.Location = new System.Drawing.Point(221, 167);
            this.successReward.Name = "successReward";
            this.successReward.Size = new System.Drawing.Size(50, 20);
            this.successReward.TabIndex = 8;
            this.successReward.TextChanged += new System.EventHandler(this.successReward_TextChanged);
            // 
            // failurePenalty
            // 
            this.failurePenalty.Location = new System.Drawing.Point(221, 194);
            this.failurePenalty.Name = "failurePenalty";
            this.failurePenalty.Size = new System.Drawing.Size(51, 20);
            this.failurePenalty.TabIndex = 9;
            this.failurePenalty.TextChanged += new System.EventHandler(this.failurePenalty_TextChanged);
            // 
            // deleteAction
            // 
            this.deleteAction.Location = new System.Drawing.Point(150, 238);
            this.deleteAction.Name = "deleteAction";
            this.deleteAction.Size = new System.Drawing.Size(54, 23);
            this.deleteAction.TabIndex = 11;
            this.deleteAction.Text = "Delete";
            this.deleteAction.UseVisualStyleBackColor = true;
            this.deleteAction.Click += new System.EventHandler(this.deleteAction_Click);
            // 
            // saveAction
            // 
            this.saveAction.Location = new System.Drawing.Point(217, 238);
            this.saveAction.Name = "saveAction";
            this.saveAction.Size = new System.Drawing.Size(54, 23);
            this.saveAction.TabIndex = 12;
            this.saveAction.Text = "Save";
            this.saveAction.UseVisualStyleBackColor = true;
            this.saveAction.Click += new System.EventHandler(this.saveAction_Click);
            // 
            // addAction
            // 
            this.addAction.Location = new System.Drawing.Point(16, 268);
            this.addAction.Name = "addAction";
            this.addAction.Size = new System.Drawing.Size(90, 23);
            this.addAction.TabIndex = 13;
            this.addAction.Text = "New Action";
            this.addAction.UseVisualStyleBackColor = true;
            this.addAction.Click += new System.EventHandler(this.addAction_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(112, 268);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(160, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "WARNING: EDITING ACTIONS";
            // 
            // mutationRate
            // 
            this.mutationRate.Location = new System.Drawing.Point(171, 92);
            this.mutationRate.Name = "mutationRate";
            this.mutationRate.Size = new System.Drawing.Size(100, 20);
            this.mutationRate.TabIndex = 17;
            this.mutationRate.TextChanged += new System.EventHandler(this.mutationRate_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 95);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Mutation Rate";
            // 
            // Success
            // 
            this.Success.AutoSize = true;
            this.Success.Location = new System.Drawing.Point(150, 170);
            this.Success.Name = "Success";
            this.Success.Size = new System.Drawing.Size(48, 13);
            this.Success.TabIndex = 18;
            this.Success.Text = "Success";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(150, 197);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Failure";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(112, 281);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(157, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "RESETS INTELLIGENCE TO 0";
            // 
            // CreatureConfigurer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 307);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Success);
            this.Controls.Add(this.mutationRate);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.addAction);
            this.Controls.Add(this.saveAction);
            this.Controls.Add(this.deleteAction);
            this.Controls.Add(this.failurePenalty);
            this.Controls.Add(this.successReward);
            this.Controls.Add(this.actionName);
            this.Controls.Add(this.damagePenalty);
            this.Controls.Add(this.deathPenalty);
            this.Controls.Add(this.maxHP);
            this.Controls.Add(this.actionList);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "CreatureConfigurer";
            this.Text = "CreatureConfigurer";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CreatureConfigurer_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox actionList;
        private System.Windows.Forms.TextBox maxHP;
        private System.Windows.Forms.TextBox deathPenalty;
        private System.Windows.Forms.TextBox damagePenalty;
        private System.Windows.Forms.TextBox actionName;
        private System.Windows.Forms.TextBox successReward;
        private System.Windows.Forms.TextBox failurePenalty;
        private System.Windows.Forms.Button deleteAction;
        private System.Windows.Forms.Button saveAction;
        private System.Windows.Forms.Button addAction;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox mutationRate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label Success;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
    }
}