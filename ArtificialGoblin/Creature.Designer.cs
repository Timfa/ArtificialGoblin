﻿namespace ArtificialGoblin
{
    partial class CreatureDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.loadButton = new System.Windows.Forms.Button();
            this.creatureName = new System.Windows.Forms.TextBox();
            this.save = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.getActionButton = new System.Windows.Forms.Button();
            this.successButton = new System.Windows.Forms.Button();
            this.failureButton = new System.Windows.Forms.Button();
            this.configure = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.hpLabel = new System.Windows.Forms.Label();
            this.doHeal = new System.Windows.Forms.Button();
            this.healInput = new System.Windows.Forms.TextBox();
            this.damageInput = new System.Windows.Forms.TextBox();
            this.doDamage = new System.Windows.Forms.Button();
            this.actionLabel = new System.Windows.Forms.Label();
            this.neutralButton = new System.Windows.Forms.Button();
            this.dealDamageButton = new System.Windows.Forms.Button();
            this.avgDamageLabel = new System.Windows.Forms.Label();
            this.damageDealtInput = new System.Windows.Forms.TextBox();
            this.damageToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // loadButton
            // 
            this.loadButton.Location = new System.Drawing.Point(197, 242);
            this.loadButton.Name = "loadButton";
            this.loadButton.Size = new System.Drawing.Size(75, 23);
            this.loadButton.TabIndex = 0;
            this.loadButton.Text = "Load";
            this.loadButton.UseVisualStyleBackColor = true;
            this.loadButton.Click += new System.EventHandler(this.loadButton_Click);
            // 
            // creatureName
            // 
            this.creatureName.Location = new System.Drawing.Point(87, 12);
            this.creatureName.Name = "creatureName";
            this.creatureName.Size = new System.Drawing.Size(185, 20);
            this.creatureName.TabIndex = 1;
            this.damageToolTip.SetToolTip(this.creatureName, "The name of the creature. Must be unique.");
            this.creatureName.TextChanged += new System.EventHandler(this.creatureName_TextChanged);
            // 
            // save
            // 
            this.save.Location = new System.Drawing.Point(116, 242);
            this.save.Name = "save";
            this.save.Size = new System.Drawing.Size(75, 23);
            this.save.TabIndex = 2;
            this.save.Text = "Save";
            this.save.UseVisualStyleBackColor = true;
            this.save.Click += new System.EventHandler(this.save_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Name";
            // 
            // getActionButton
            // 
            this.getActionButton.Location = new System.Drawing.Point(16, 138);
            this.getActionButton.Name = "getActionButton";
            this.getActionButton.Size = new System.Drawing.Size(256, 23);
            this.getActionButton.TabIndex = 5;
            this.getActionButton.Text = "Get Action";
            this.damageToolTip.SetToolTip(this.getActionButton, "Get a creature action for its combat turn.");
            this.getActionButton.UseVisualStyleBackColor = true;
            this.getActionButton.Click += new System.EventHandler(this.getActionButton_Click);
            // 
            // successButton
            // 
            this.successButton.Location = new System.Drawing.Point(16, 184);
            this.successButton.Name = "successButton";
            this.successButton.Size = new System.Drawing.Size(75, 23);
            this.successButton.TabIndex = 6;
            this.successButton.Text = "Good idea";
            this.damageToolTip.SetToolTip(this.successButton, "Positively reward this behaviour.\r\nClick this if the action of the creature had a" +
        "n overall good effect.");
            this.successButton.UseVisualStyleBackColor = true;
            this.successButton.Click += new System.EventHandler(this.successButton_Click);
            // 
            // failureButton
            // 
            this.failureButton.Location = new System.Drawing.Point(197, 184);
            this.failureButton.Name = "failureButton";
            this.failureButton.Size = new System.Drawing.Size(75, 23);
            this.failureButton.TabIndex = 7;
            this.failureButton.Text = "Bad idea";
            this.damageToolTip.SetToolTip(this.failureButton, "Punish this behaviour.\r\nClick this if the creature\'s action had an overall negati" +
        "ve effect.");
            this.failureButton.UseVisualStyleBackColor = true;
            this.failureButton.Click += new System.EventHandler(this.failureButton_Click);
            // 
            // configure
            // 
            this.configure.Location = new System.Drawing.Point(16, 242);
            this.configure.Name = "configure";
            this.configure.Size = new System.Drawing.Size(75, 23);
            this.configure.TabIndex = 8;
            this.configure.Text = "Configure";
            this.configure.UseVisualStyleBackColor = true;
            this.configure.Click += new System.EventHandler(this.configure_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(113, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "HP";
            // 
            // hpLabel
            // 
            this.hpLabel.AutoSize = true;
            this.hpLabel.Location = new System.Drawing.Point(113, 96);
            this.hpLabel.Name = "hpLabel";
            this.hpLabel.Size = new System.Drawing.Size(13, 13);
            this.hpLabel.TabIndex = 10;
            this.hpLabel.Text = "0";
            // 
            // doHeal
            // 
            this.doHeal.Location = new System.Drawing.Point(16, 91);
            this.doHeal.Name = "doHeal";
            this.doHeal.Size = new System.Drawing.Size(75, 23);
            this.doHeal.TabIndex = 11;
            this.doHeal.Text = "Heal";
            this.doHeal.UseVisualStyleBackColor = true;
            this.doHeal.Click += new System.EventHandler(this.doHeal_Click);
            // 
            // healInput
            // 
            this.healInput.Location = new System.Drawing.Point(16, 65);
            this.healInput.Name = "healInput";
            this.healInput.Size = new System.Drawing.Size(75, 20);
            this.healInput.TabIndex = 12;
            this.damageToolTip.SetToolTip(this.healInput, "Enter the HP gained by healing this creature here.");
            this.healInput.TextChanged += new System.EventHandler(this.healInput_TextChanged);
            // 
            // damageInput
            // 
            this.damageInput.Location = new System.Drawing.Point(197, 65);
            this.damageInput.Name = "damageInput";
            this.damageInput.Size = new System.Drawing.Size(75, 20);
            this.damageInput.TabIndex = 14;
            this.damageToolTip.SetToolTip(this.damageInput, "Enter the HP lost by damage to the creature here.");
            this.damageInput.TextChanged += new System.EventHandler(this.damageInput_TextChanged);
            // 
            // doDamage
            // 
            this.doDamage.Location = new System.Drawing.Point(197, 91);
            this.doDamage.Name = "doDamage";
            this.doDamage.Size = new System.Drawing.Size(75, 23);
            this.doDamage.TabIndex = 13;
            this.doDamage.Text = "Damage";
            this.doDamage.UseVisualStyleBackColor = true;
            this.doDamage.Click += new System.EventHandler(this.doDamage_Click);
            // 
            // actionLabel
            // 
            this.actionLabel.AutoSize = true;
            this.actionLabel.Location = new System.Drawing.Point(16, 168);
            this.actionLabel.Name = "actionLabel";
            this.actionLabel.Size = new System.Drawing.Size(31, 13);
            this.actionLabel.TabIndex = 15;
            this.actionLabel.Text = "none";
            this.damageToolTip.SetToolTip(this.actionLabel, "This is the action the creature wants to make this turn.\r\nNote that ignoring this" +
        " action and making the creature do something else will negatively impact the lea" +
        "rning process.");
            // 
            // neutralButton
            // 
            this.neutralButton.Location = new System.Drawing.Point(107, 184);
            this.neutralButton.Name = "neutralButton";
            this.neutralButton.Size = new System.Drawing.Size(75, 23);
            this.neutralButton.TabIndex = 16;
            this.neutralButton.Text = "Neutral";
            this.damageToolTip.SetToolTip(this.neutralButton, "Neutral action.\r\nClick this if the creature\'s action neither had a positive or ne" +
        "gative impact.");
            this.neutralButton.UseVisualStyleBackColor = true;
            this.neutralButton.Click += new System.EventHandler(this.neutralButton_Click);
            // 
            // dealDamageButton
            // 
            this.dealDamageButton.Location = new System.Drawing.Point(96, 213);
            this.dealDamageButton.Name = "dealDamageButton";
            this.dealDamageButton.Size = new System.Drawing.Size(95, 23);
            this.dealDamageButton.TabIndex = 17;
            this.dealDamageButton.Text = "Deal Damage";
            this.damageToolTip.SetToolTip(this.dealDamageButton, "Enter the damage a creature dealt to a player here.\r\nIf the creature missed an at" +
        "tack, enter \"0\".");
            this.dealDamageButton.UseVisualStyleBackColor = true;
            this.dealDamageButton.Click += new System.EventHandler(this.dealDamageButton_Click);
            // 
            // avgDamageLabel
            // 
            this.avgDamageLabel.AutoSize = true;
            this.avgDamageLabel.Location = new System.Drawing.Point(197, 218);
            this.avgDamageLabel.Name = "avgDamageLabel";
            this.avgDamageLabel.Size = new System.Drawing.Size(13, 13);
            this.avgDamageLabel.TabIndex = 18;
            this.avgDamageLabel.Text = "0";
            // 
            // damageDealtInput
            // 
            this.damageDealtInput.Location = new System.Drawing.Point(16, 215);
            this.damageDealtInput.Name = "damageDealtInput";
            this.damageDealtInput.Size = new System.Drawing.Size(75, 20);
            this.damageDealtInput.TabIndex = 19;
            this.damageToolTip.SetToolTip(this.damageDealtInput, "Enter the damage a creature dealt to a player here.");
            this.damageDealtInput.TextChanged += new System.EventHandler(this.damageDealtInput_TextChanged);
            // 
            // CreatureDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 275);
            this.Controls.Add(this.damageDealtInput);
            this.Controls.Add(this.avgDamageLabel);
            this.Controls.Add(this.dealDamageButton);
            this.Controls.Add(this.neutralButton);
            this.Controls.Add(this.actionLabel);
            this.Controls.Add(this.damageInput);
            this.Controls.Add(this.doDamage);
            this.Controls.Add(this.healInput);
            this.Controls.Add(this.doHeal);
            this.Controls.Add(this.hpLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.configure);
            this.Controls.Add(this.failureButton);
            this.Controls.Add(this.successButton);
            this.Controls.Add(this.getActionButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.save);
            this.Controls.Add(this.creatureName);
            this.Controls.Add(this.loadButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "CreatureDialog";
            this.Text = "Creature";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CreatureDialog_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Creature_FormClosed);
            this.Move += new System.EventHandler(this.CreatureDialog_Move);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button loadButton;
        private System.Windows.Forms.TextBox creatureName;
        private System.Windows.Forms.Button save;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button getActionButton;
        private System.Windows.Forms.Button successButton;
        private System.Windows.Forms.Button failureButton;
        private System.Windows.Forms.Button configure;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label hpLabel;
        private System.Windows.Forms.Button doHeal;
        private System.Windows.Forms.TextBox healInput;
        private System.Windows.Forms.TextBox damageInput;
        private System.Windows.Forms.Button doDamage;
        private System.Windows.Forms.Label actionLabel;
        private System.Windows.Forms.Button neutralButton;
        private System.Windows.Forms.Button dealDamageButton;
        private System.Windows.Forms.Label avgDamageLabel;
        private System.Windows.Forms.TextBox damageDealtInput;
        private System.Windows.Forms.ToolTip damageToolTip;
    }
}